const moment = require('moment-timezone')

const repoEx = getmodule('db/externo/repository')
const repoPr = getmodule('db/proprio/repository');

const bpa = getmodule('jobs/buscaPorAbastecimento')

/* ==============================================================
    Obtém status consolidado das instalações ao final dos dias
=================================================================*/
async function registroDeHistorico() {
    try {
        let replication = await repoEx.obterReplicacaoDBHistorico();

        if (replication.length > 0) { /**Se possui uma tarefa de replicacao ativa */
            if (replication[0].changes_pending >= 0) { /**Se ja replicou as pendencias */
                let dtExec = new moment().tz("America/Sao_Paulo");
                try {
                    let ultExec = await repoPr.obterUltimoRegistroHistorico();
                    if (ultExec.rows.length != 0) { /**Se possui algum registro de historico */
                        ultExec = ultExec.rows[0].value.dtExec;
                        ultExec = moment(ultExec);
                    }
                    else {
                        ultExec = moment(new Date(2017, 7, 25, 18, 0, 0));
                    }
                    while (dtExec.diff(ultExec, "days") > 0) {
                        ultExec.add(1, "day");

                        try {
                            var listaPVs = await repoPr.obterInstalacoesAtivas(ultExec.format());
                            pvsStatus = await Promise.all(listaPVs.map(async (pv) => {
                                return bpa.atribuirUltAbast(pv, ultExec.format());
                            }));

                            let pvsAtivos = pvsStatus.filter(pv => (ultExec.diff(moment(pv.ultimoAbastecimento), 'hours') == 0));
                            let pvsInativos = pvsStatus.filter(pv => (ultExec.diff(moment(pv.ultimoAbastecimento), 'days') >= 1))
                            let pvsWarning = pvsStatus.filter(pv => (ultExec.diff(moment(pv.ultimoAbastecimento), 'hours') <= 23 && ultExec.diff(moment(pv.ultimoAbastecimento), 'hours') > 0))

                            let hist = {
                                dtExec: ultExec.format(),
                                total: pvsStatus.length,
                                pvsAtivos: pvsAtivos.length,
                                pvsWarning: pvsWarning.length,
                                pvsInativos: pvsInativos.length
                            }
                            repoPr.postHistoricoInstalacao(hist);
                        }
                        catch (err) {
                            console.log("Não foi possível efetuar o registro do historico", err);
                        }
                    }
                }
                catch (err) {
                    console.log("Não foi possível obter o último registro do historico", err);
                }
            }
        }
        else {
            //repoEx.postReplicacaoDBHistorico();
        }
    }
    catch (err) {
        console.log("Não foi possivel obter a replicação do historico", err);
    }

}

module.exports = { registroDeHistorico }