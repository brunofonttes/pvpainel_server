
const repoPr = getmodule('db/proprio/repository');
const env = getmodule('config/environment').current;

function compactacaoLPV() {
    repoPr.postCompactarDB(env.dbpv);
}

module.exports = { compactacaoLPV };