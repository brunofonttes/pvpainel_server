﻿const moment = require('moment-timezone');

const repoEx = getmodule('db/externo/repository');
const repoPr = getmodule('db/proprio/repository');

/*====================================================================
                        Funçao auxiliar
======================================================================*/
function isvalidPV(npv) {
    try {
        if (npv != undefined && npv != null) {
            return true;
        }
        return false;
    }
    catch (err) {
        return false;
    }
}

/* ========================================================================================
        Job de execução diária capaz de criar o arquivo lpvs.json caso não exista,
        realizar uma primeira execução varrendo toda a base e na sequência, já
        existindo uma lista armazenada, verificar apenas os registros contidos
        no dia anterior.
===========================================================================================*/

async function atualizacaoLPV() {
    var dtExec = new moment().tz("America/Sao_Paulo");
    var instalExist = []
    var instalAtivasNoPeriodo = [];

    let ultExec = await repoPr.obterUltimaAtualizacao();
    /*Verifica se ocorreu algum problema ao acessar a base de registro de execuções */
    console.log(ultExec)
    if (ultExec != null) {
        /*Verifica se existe algum registro de última execução */
        if (ultExec != 0) {
            instalExist = await repoPr.obterInstalacoesAtivas(); /* obtem instalações ativas */
            if (instalExist != null) {/* Verifica se houve algum problema ao acessar a base de instalações propria */
                if (instalExist.length > 0) {
                    var dataIn = moment(ultExec).format('YYYYMMDD');
                    var dataFim = moment(dtExec).format('YYYYMMDD');
                    console.log(dataIn,dataFim)

                    instalAtivasNoPeriodo = await repoEx.obterInstalacoesAtivasNoPeriodo(dataIn, dataFim);
                    if (instalAtivasNoPeriodo != null) {
                        consolidaLPVs(instalExist, instalAtivasNoPeriodo, dtExec);
                        repoPr.postAtualizacaoPV(dtExec.format());
                    }
                    else {
                        console.log("Houve algum problema ao acessar a base dbhistorico.");
                    }
                }
                else {
                    instalAtivasNoPeriodo = await repoEx.obterInstalacoesAtivasNoPeriodoCompleto();
                    if (instalAtivasNoPeriodo != null) {
                        consolidaLPVs(instalExist, instalAtivasNoPeriodo, dtExec);
                        repoPr.postAtualizacaoPV(dtExec.format());
                    }
                    else {
                        console.log("Houve algum problema ao acessar a base dbhistorico.");
                    }
                }
            }
            else {
                console.log("Houve algum problema ao acessar a base dbpv(exec/prod).");
            }
        }
        else {
            var instalAtivasNoPeriodo = await repoEx.obterInstalacoesAtivasNoPeriodoCompleto();
            if (instalAtivasNoPeriodo != null) {
                consolidaLPVs(instalExist, instalAtivasNoPeriodo, dtExec);
                repoPr.postAtualizacaoPV(dtExec.format());
            }
            else {
                console.log("Houve algum problema ao acessar a base dbhistorico.");
            }

        }
    }
    else{
        console.log("Houve algum problema ao acessar a base dbexecucao.");
    }
}

/* ===========================================================================================
        Verifica diferença entre a lista de PVs existente armazenada e a lista de PVs 
        obtida em "obterLPVs" gerando uma nova lista de PVs atualizada.
===========================================================================================*/
async function consolidaLPVs(instalExist, instalAtivasNoPeriodo, dtExec) {
    instalExist = instalExist.map(pv => pv.npv);

    let novasInstal = instalAtivasNoPeriodo.filter(npv => ((instalExist.indexOf(npv) == -1))); /**Obtém PVs ativos no dia que não estejam na lista de PVs já existentes */
    novasInstal = novasInstal.filter(isvalidPV);/*Verifica se é um PV válido */

    if (novasInstal.length != 0) {
        let dtBusca = moment(dtExec).add(6, 'hour').format();
        novasInstal = novasInstal.map(npv => { /**Mapeia as novas instalacoes*/
            return {
                npv: npv,
                alias: '-',
                pf: 0,
                ultimoAbastecimento: '-',
                primeiroAbastecimento: '-',
                status:1
            }
        });
        console.log(novasInstal);
        try{
            novasInstal = await Promise.all(novasInstal.map(async (pv) => {
                return atribuirPrimAbast(pv, dtBusca);
            }));
            novasInstal.forEach(repoPr.postInstalacao);
        }
        catch(err){
            console.log("Não foi possível obter todos os primeiros abastecimentos",err);
        }
    }
}

function atribuirPrimAbast(pv, dtBusca) {
    return new Promise(async (resolve, reject) => {
        try {
            pv.primeiroAbastecimento = await repoEx.obterPrimAbast(pv.npv, dtBusca);
            resolve(pv);
        }
        catch (err) {
            reject('ERRO ao conseguir o primeiro Abastecimento', err);
        }
    })
}

module.exports = { atualizacaoLPV }