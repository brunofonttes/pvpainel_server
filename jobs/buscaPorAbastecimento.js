﻿const moment = require('moment-timezone')

const repoEx = getmodule('db/externo/repository')
const repoPr = getmodule('db/proprio/repository');

async function buscaPorAbastecimento(io) {
	/* ==========================================================
		Obtém status das instalações no momento atual 
	==========================================================*/
	let replication = await repoEx.obterReplicacaoDBHistorico();
	if(replication != null){
		if (replication.length > 0) { /**Se possui uma tarefa de replicacao ativa */
			if (replication[0].changes_pending >= 0) {/**Se já terminou de replicar */
	
				let dtExec = new moment().tz("America/Sao_Paulo");
				var listaPVs = await repoPr.obterInstalacoesAtivas();
	
				if (listaPVs != null) { /**Se houve algum problema ao acessar a base de pvs */
					listaPVs = listaPVs.filter(pv => pv.status!=-1);
					
					let pvsStatus = await Promise.all(listaPVs.map(async (pv) => {
						return atribuirUltAbast(pv, dtExec.format());
					}));
					pvsStatus = pvsStatus.map(pv => {
						return({
							npv:pv.npv,
							alias:pv.alias,
							pf:pv.pf,
							primeiroAbastecimento:pv.primeiroAbastecimento,
							ultimoAbastecimento:pv.ultimoAbastecimento,
							status:pv.status
						})
					})
					//console.log(pvsStatus);
					pvsStatus.forEach(repoPr.atualizaDadosPV); /**Atualiza data do ultimo abastecimento */
					
					repoPr.postExecucao(dtExec.format());	/**Insere novo registro de execucao */
	
					/** ========== Envia dados para  o painel ========== */
					let json = {
						hrAtt: dtExec.format(),
						pvs: pvsStatus
					}

					io.emit('infoPVs', json);
				}
				else{
					console.log("Houve algum problema ao acessar a base dbpv(exec/prod).");
				}
			}
		}
		else {
			repoEx.postReplicacaoDBHistorico();
		}
	}
	else{
		console.log("Houve algum problema ao acessar o CouchDB");
	}
	
}


function atribuirUltAbast(pv, dtExec) {
	return new Promise(async (resolve, reject) => {
		try {
			pv.ultimoAbastecimento = await repoEx.obterUltAbast(pv.npv, dtExec);
			resolve(pv);
		}
		catch (err) {
			console.log(pv, dtExec ,err)
			reject('Não foi possível obter o ultimo abastecimento');
		}
	})
}

module.exports = { buscaPorAbastecimento, atribuirUltAbast }