var pvsOff = [] //com tempo sem abastecer
var pvsPF = [] // PVs com Pro-Frotas
var pvs = []

var socket = io();

/**====================================================================================*/
socket.on('infoPVs', function (TST) {
    loadStatus(TST);
})

function loadStatus(json) {
    let dtExec = moment(json.hrAtt);
    let fdtExec = dtExec.format('DD/MM/YYYY LT');
    $('#hrSinc').text('Última Sincronização: ' + fdtExec);

    pvs = json.pvs;
    pvsOff = filterlpvsOff(pvs, dtExec);
    pvsOff = pvsOff.sort(function (a, b) { return b.tpSAbst - a.tpSAbst; });
    pvsOff = pvsOff.filter(pv => pv.tpSAbst > 0 || pv.tpSAbst == -1);
    
    filterBlockedLPVs(false);
}

/**====================================================================================*/
function filterlpvsOff(pvs, dtExec) {
    //retira resultados nulos dos pvs
    pvs = pvs.map(pv => {
        let tpSAbst = -1
        if (moment(pv.ultimoAbastecimento).isValid() == true) {
            let ultimoAbastecimento = moment(pv.ultimoAbastecimento);
            tpSAbst = dtExec.diff(ultimoAbastecimento, 'hour');
        }
        return {
            npv: pv.npv,
            alias: pv.alias,
            pf: pv.pf,
            ultimoAbastecimento: pv.ultimoAbastecimento,
            tpSAbst: tpSAbst,
            status: pv.status
        }
    });
    return pvs;  
}

function loadlpvs(pvsOff) {
    $('#messages li').remove();
    let qtdY = defineColunas(pvsOff);
    insereDonutsHTML(pvsOff);
    insereInfoDonHTML(pvsOff);
    let fs = setaProporcaoInfo(qtdY)
    setaProporcaoDonuts(fs, qtdY);
}

/**====================================================================================*/
function filterBlockedLPVs(check) {
    if (check == false) {
        $("#oper").prop('checked',true);
        let pvsFiltered = pvsOff.filter(pv => pv.status != 0);
        let pvsBlocked = pvsOff.filter(pv => pv.status == 0);
        loadlpvs(pvsFiltered);
        loadprogressbar(pvs, pvsOff, pvsBlocked);
    }
    else {
        loadlpvs(pvsOff);
        loadprogressbar(pvs, pvsOff);
    }
}

function filterOperatingLPVs(check){
    if(check == false){
        $("#block").prop('checked',true);
        let pvsFiltered = pvsOff.filter(pv => pv.status == 0);
        let pvsBlocked = pvsOff.filter(pv => pv.status == 0);
        loadlpvs(pvsFiltered);
        loadprogressbar(pvs, pvsOff, pvsBlocked);
    }
    else{
        loadlpvs(pvsOff);
        loadprogressbar(pvs, pvsOff);
    }
}
/**====================================================================================*/

$(window).on('resize', function () {
    $('#messages li').remove()
    if (pvsOff.length > 0) {
        qtdY = defineColunas(pvsOff);
        insereDonutsHTML(pvsOff);
        insereInfoDonHTML(pvsOff);
        let fs = setaProporcaoInfo(qtdY)
        setaProporcaoDonuts(fs, qtdY);
    }
})



