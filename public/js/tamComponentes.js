function defineColunas(data) {
    var pvs = data
    var col;
    if ($(window).width() > $(window).height()) {
        col = Math.ceil(pvs.length / (Math.sqrt(pvs.length * 0.8 * $(window).width() / $(window).height())))
        $('ul').css('-webkit-column-count', col);
        $('ul').css('-moz-column-count', col);
        $('ul').css('column-count', col);
    }
    else {
        col = 0;
    }
    console.log(col)
    var qtdY = Math.ceil(pvs.length / col)
    return qtdY;
}


function setaProporcaoInfo(qtdY) {
    let fontsize = 0.22 * $('#messages').height() / (qtdY+1); /*trocar nome, fontsize nao é um bom nome */
    $('.info').css('font-size', fontsize + 'px')
    return fontsize;
}

function setaProporcaoDonuts(fontsize, qtdY) {
    $('.dados').css('width', $('#messages').height() / (qtdY+1) + 'px');
    $('.bloco').css('height', $('#messages').height() / (qtdY+1) + 'px');
    $('.info').css('top',-($('.dados').height()-$('#info').height())/4)
}
