function circlifulConfig(pos, tpSAbst) {
	let day = false;
	let month = false;
	let pcent = 100;
	if (tpSAbst >= 24) {
		tpSAbst = Math.floor(tpSAbst / 24, 0);
		if (tpSAbst > 60) {
			month = true;
			tpSAbst = Math.floor(tpSAbst / 30);
			pcent = 100;
		}
		else {
			day = true;
			pcent = Math.round(tpSAbst * 100 / 7);
		}
	}
	else {
		pcent = Math.round(tpSAbst * 100 / 23);
	}

	function progressColorChange() {
		let progressColorPar = { 0: '#04fbaa', 25: '#ffce00' }
		if (day || month) {
			progressColorPar = { 0: '#ef6729', 50: '#e4484d', 100: '#d8373c' }
		}
		return progressColorPar
	}
	
	function replacePercentageByTextChange() {
		if (day) { return tpSAbst + 'd'; }
		if (month) { return tpSAbst + 'M'; }
		return tpSAbst + 'h';
	}

	progressColor = progressColorChange();
	replacePercentageByText = replacePercentageByTextChange();

	$(".dados:eq(" + pos + ")").circliful({
		animation: 1,
		animationStep: 6,
		foregroundBorderWidth: 8,
		backgroundBorderWidth: 15,
		foregroundColor: '#d8373c',
		backgroundColor: '#2b3e4f',
		multiplePercentage: 1,
		progressColor: progressColor,
		percent: pcent,
		replacePercentageByText: replacePercentageByText,
		iconPosition: 'top',
		textAdditionalCss: 'font-size:2.3em',
		percentageY: 112,
		textStyle: ' font-size:1.2em;',
	});
}

function insereDonutsHTML(pvsOff) {
	for (let pv = 0; pv < pvsOff.length; pv++) {
		let tpSAbst = pvsOff[pv].tpSAbst;
		$('#messages').append(
			$('<li>' +
				'<div class= "bloco">' +
				'<div class = "dados"></div>' +
				'</div>' +
				'</li>'
			))
		circlifulConfig(pv, tpSAbst);
	}
}

function insereInfoDonHTML(pvsOff) {
	for (let pv = 0; pv < pvsOff.length; pv++) {
		let nomePV = pvsOff[pv].alias;
		let npv = pvsOff[pv].npv;
		let bold = '';

		if (pvsOff[pv].pf == 2 || pvsOff[pv].pf == 3) {
			bold = "bold";
		}

		$(".bloco:eq(" + pv + ")").append($(
			'<div class = "info ' + bold + ' ">' +
			'<span class="nome">' + nomePV + '</span><br>' +
			'<span class="pv">' + npv + '</span>' +
			'</div>'
		))
		
		if (nomePV.length >= 12) {
			$('.nome:last').css('font-size', 12 / nomePV.length + 'em')
		}
	}
}