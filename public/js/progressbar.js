//Atualiza quantidade de PVs On
function updateQtdOn(qtd, total) {
    let pcent = qtd * 100 / total;
    $('.progress-bar-on').css('width', pcent.toFixed() + "%")

    if (pcent > 5) { $('.progress-bar-on').text(qtd + " On (" + Math.round(pcent) + "%)") }
    else if (pcent != 0) { 
        if (pcent > 2.5) {
            $('.progress-bar-on').text(qtd + " On") ;
        }
        else{
            $('.progress-bar-on').text(qtd); 
        }
    }
}

function updateQtdOff(qtd, total) {
    let pcent = qtd * 100 / total;
    $('.progress-bar-off').css('width', pcent + "%")

    if (pcent > 5) { $('.progress-bar-off').text(qtd + " Off (" + Math.round(pcent) + "%)") }
    else if (pcent != 0) {
        if (pcent > 2.5) {
            $('.progress-bar-off').text(qtd + " Off");
        }
        else {
            $('.progress-bar-off').text(qtd);
        }
    }
}

function updateQtdWarn(qtd, total) {
    let pcent = qtd * 100 / total;
    $('.progress-bar-warning').css('width', pcent + "%")
    if (pcent > 5) { $('.progress-bar-warning').text(qtd + " Warn (" + Math.round(pcent) + "%)") }
    else if (pcent != 0) {
        if (pcent > 2.5) {
            $('.progress-bar-warning').text(qtd + " Wrn");
        }
        else {
            $('.progress-bar-warning').text(qtd);
        }
    }
}

function updateQtdBlockedOff(qtd, total) {
    let pcent = qtd * 100 / total;
    $('.progress-bar-blocked-off').css('width', pcent + "%")

    if (pcent > 5) { $('.progress-bar-blocked-off').text(qtd + " Bloq (" + Math.round(pcent) + "%)") }
    else if (pcent != 0) { 
        if (pcent > 2.5) {
            $('.progress-bar-blocked-off').text(qtd + " Bl") 
        }
        else{
            $('.progress-bar-blocked-off').text(qtd);
        }
    }
}

function updateQtdBlockedWarning(qtd, total) {
    let pcent = qtd * 100 / total;
    $('.progress-bar-blocked-warning').css('width', pcent + "%")

    if (pcent > 10) { $('.progress-bar-blocked-warning').text(qtd + " Bloq (" + Math.round(pcent) + "%)") }
    else if (pcent != 0) { 
        if (pcent > 2.5) {
            $('.progress-bar-blocked-warning').text(qtd + " Bl") 
        }
        else{
            $('.progress-bar-blocked-warning').text(qtd);
        }
    }
}

function loadprogressbar(pvs, pvsOff, pvsBlocked) {
    let pvsBlockedWarn;
    let pvsBlockedOff;

    if (pvsBlocked === undefined) {
        pvsBlockedWarn = 0;
        pvsBlockedOff = 0;
    }
    else {
        pvsBlockedWarn = (pvsBlocked.filter(pv => pv.tpSAbst < 24)).length;
        pvsBlockedOff = pvsBlocked.length - pvsBlockedWarn;
    }

    let pvsWarn = (pvsOff.filter(pv => pv.tpSAbst < 24)).length;
    let pvsOn = pvs.length - pvsOff.length;
    let pvsInat = (pvsOff.length - pvsWarn) - pvsBlockedOff;

    pvsWarn = pvsWarn - pvsBlockedWarn;/**Necessário para realizar a conta de pvsInativos corretamente */

    updateQtdOn(pvsOn, pvs.length);
    updateQtdOff(pvsInat, pvs.length);
    updateQtdBlockedOff(pvsBlockedOff, pvs.length);
    updateQtdBlockedWarning(pvsBlockedWarn, pvs.length);
    updateQtdWarn(pvsWarn, pvs.length);

}