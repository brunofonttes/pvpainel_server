const prod = {
    dbpv:'dbpv',
    dbexecucao:'dbexecucao',
    dbatualizacao:'dbatualizacao',
    dbpvhistorico:'dbpvhistorico',
    dbexclusao:'dbexclusao'
}

const dev = {
    dbpv:'dbpv_dev',
    dbexecucao:'dbexecucao_dev',
    dbatualizacao:'dbatualizacao_dev',
    dbpvhistorico:'dbpvhistorico_dev',
    dbexclusao:'dbexclusao'
}

const current = prod

module.exports = {current}
