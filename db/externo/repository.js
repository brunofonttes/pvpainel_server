﻿const dbpa = getmodule('db/externo/endpoint')
const request = require('request')
const moment = require('moment')
const daoEx = getmodule('db/externo/dao')

function asyncParse(string) {
	return (new Response(string)).json();
}

/* ===========================================================================================
         Realiza requisição para a origem verificando a partir do número do PV 
         Corporativo, a quantidade de abastecimentos em um dado período.
  ===========================================================================================*/
function obterAbast(doc) {
	return new Promise((resolve, reject) => {
		try {
			doc = JSON.parse(doc);
			let dateFromDoc = doc.rows[0].value.data;
			if (moment(dateFromDoc).isValid()) {
				let dateAbastecimento = moment(dateFromDoc).tz('America/Sao_Paulo').add(3, 'hour').format();
				resolve(dateAbastecimento);
			}
			else {
				reject(null);
			}
		}
		catch (err) {
			reject(null);
		}
	})
}

async function obterUltAbast(npv, dtExec) {
	try{
		let doc = await daoEx.obterUltAbast(npv, dtExec);
		return obterAbast(doc);
	}
	catch(err){
		console.log(err);
		return null;
	}
	
}

async function obterPrimAbast(npv, dtExec) {
	try{
		let doc = await daoEx.obterPrimAbast(npv, dtExec);
		return obterAbast(doc);
	}
	catch(err){
		console.log(err);
		return(null);
	}

}

/*================================================================================================== */
function obterInstalacoesAtivas(doc) {
	return new Promise((resolve, reject) => {
		try {
			doc = JSON.parse(doc);
			let pvsAtivosNoPeriodo = [...new Set(doc.rows.map(row => row.value.npv))];
			resolve(pvsAtivosNoPeriodo);
		}
		catch (err) {
			reject(null);
		}
	});
}

async function obterInstalacoesAtivasNoPeriodo(inicio, fim) {
	try{
		let doc = await daoEx.obterInstalacoesAtivasNoPeriodo(inicio, fim);
		return obterInstalacoesAtivas(doc);
	}
	catch(err){
		console.log(err);
		return null;
	}
	
	
}

async function obterInstalacoesAtivasNoPeriodoCompleto() {
	try{
		let doc = await daoEx.obterInstalacoesAtivasNoPeriodoCompleto();
		return obterInstalacoesAtivas(doc);
	}
	catch(err){
		console.log(err);
		return null;
	}
}

/*================================================================================================== */
async function obterReplicacaoDBHistorico() {
	let doc = await daoEx.obterTarefasAtivas();
	return new Promise((resolve, reject) => {
		try {
			doc = JSON.parse(doc);
			let tarefasAtivas = doc.filter(task => {
				return (task.type === 'replication') && 
				(task.source === 'http://couchdb-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br/dbhistorico/')&&
				(task.target === 'http://127.0.0.1:5984/dbhistorico/')
			});
			resolve(tarefasAtivas);
		}
		catch (err) {
			console.log(err);
			reject(null);
		}
	})
}

async function postReplicacaoDBHistorico() {
	let data = {
		continuous:true,
        source:'http://couchdb-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br/dbhistorico/',
        target:'http://127.0.0.1:5984/dbhistorico/'
	}
	data = JSON.stringify(data);
	daoEx.postReplicacaoDBHistorico(data);
}

module.exports = {
	obterUltAbast,
	obterPrimAbast,
	obterInstalacoesAtivasNoPeriodo,
	obterInstalacoesAtivasNoPeriodoCompleto,
	obterReplicacaoDBHistorico,
	postReplicacaoDBHistorico
};