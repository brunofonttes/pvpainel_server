const url_base = "http://couchdb-replication-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br/dbhistorico/_design/clebio/_view";
const endpoint_base = "http://couchdb-replication-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br";

function createKey(pv, data){
    return [pv,"\""+data+"\""];
}

function abast_URL(pv, dtExec, desc) {
    if (desc == true){
        key = "startkey=["+createKey(pv,dtExec)+"]&endkey=["+pv+"]";
    }
    else{
        key = "startkey=["+pv+"]&endkey=["+createKey(pv,dtExec)+"]";
    }
    url = url_base+"/painelalerta?"+key+"&limit=1&descending="+desc;
    return url
}


function pvsAtivosNoPeriodo_URL(dataIn, dataFim){
  
    url = url_base + '/painelalertapdvkmv?startkey=['+dataIn+',0,0]&endkey=['+dataFim+',99999999,4]&reduce=false';
    console.log(url)
    return url
}

function  pvsAtivosNoPeriodoCompleto_URL(){
    url = url_base + '/painelalertapdvkmv?reduce=false';
    return url
}

/* ========================== Tasks ======================== */
function tarefasAtivas_URL(){
    url = endpoint_base + "/_active_tasks/";
    return url;
}

function replicateDBHistorico_URL(){
    url = endpoint_base + "/_replicate/";
    return url; 
}

module.exports = { 
    pvsAtivosNoPeriodo_URL, 
    abast_URL,   
    pvsAtivosNoPeriodoCompleto_URL,
    tarefasAtivas_URL,
    replicateDBHistorico_URL
}

