const request = require('request');
const endpoint = getmodule('db/externo/endpoint');

const username = "clebio";
const password = "clebio";

const auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

/*=================== Função de Base/Raiz ===============================*/
function obterDados(url) {
    return new Promise((resolve, reject) => {
        request.get(url, (error, response, body) => {
            resolve(body);
        })
        .on('error', err => {
            reject("obterDados:",err);
        })
    })
}

function gravarDados(url,body) {
    request.post({
        headers: {
            'content-type': 'application/json',
            "Authorization": auth
        },
        url: url,
        body: body,
    }, function (error, response, body) {
        console.log('post',url,response);
    });
}

/*==================== Funções Principais ================================*/
function obterUltAbast(npv, dtExec) {
    const prim_or_ult = true;
    const url = endpoint.abast_URL(npv, dtExec, prim_or_ult);
    console.log(url)
    return obterDados(url);
}


function obterPrimAbast(npv, dtExec) {
    const prim_or_ult = false;
    const url = endpoint.abast_URL(npv, dtExec, prim_or_ult);
    return obterDados(url);
}

async function obterInstalacoesAtivasNoPeriodo(inicio, fim) {
    const url = endpoint.pvsAtivosNoPeriodo_URL(inicio, fim);
    return obterDados(url);
}

async function obterInstalacoesAtivasNoPeriodoCompleto(inicio, fim) {
    const url = endpoint.pvsAtivosNoPeriodoCompleto_URL(inicio, fim);
    return obterDados(url);
}
/*========================= Tasks ================================= */
async function obterTarefasAtivas(){
    const url = endpoint.tarefasAtivas_URL();
    return obterDados(url);
}

async function postReplicacaoDBHistorico(data){
    // const url = endpoint.replicateDBHistorico_URL();
    // return gravarDados(url,data);
}

module.exports = { 
    obterUltAbast, 
    obterPrimAbast, 
    obterInstalacoesAtivasNoPeriodo, 
    obterInstalacoesAtivasNoPeriodoCompleto,
    obterTarefasAtivas,
    postReplicacaoDBHistorico
}