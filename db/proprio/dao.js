
const request = require('request')

const endpointPr = getmodule('db/proprio/endpoint');
const env = getmodule('config/environment').current;

const username = "clebio";
const password = "clebio";

const auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

/*=================================================================================
                                GET/POST BASE                                      
 ==================================================================================*/
function obterDados(url) {
    return new Promise((resolve, reject) => {
        request.get(url, (error, response, body) => {
            resolve(body);
        })
            .on('error', err => {
                reject("obterDados:Não foi possível acessar:", url);
            })
    })
}


function gravarDados(url, body) {
    request.post({
        headers: {
            'content-type': 'application/json',
            "Authorization": auth
        },
        url: url,
        body: body,
    }, function (err, response, body) {
        console.log("\n"+url+"\n"+body);
    });
}


function atualizarDados(url, body) {
    request.put({
        headers: {
            'content-type': 'application/json',
            "Authorization": auth
        },
        url: url,
        body: body,
    }, function (err, response, body) {
        // console.log("\n"+url+"\n"+body);
    });
}

/*=================================================================================
                                     PUT                             
 ==================================================================================*/
function putPV(data, doc_id) {
    let url = endpointPr.npvDoc_URL(doc_id);
    atualizarDados(url, data);
}

/*=================================================================================
                                     POST                                   
 ==================================================================================*/
function postInstalacao(data) {
    let url = endpointPr.bd_URL(env.dbpv);
    gravarDados(url, data);
}

function postExecucao(data) {
    let url = endpointPr.bd_URL(env.dbexecucao);
    gravarDados(url, data);
}

function postAtualizacaoPV(data){
    let url = endpointPr.bd_URL(env.dbatualizacao);
    gravarDados(url, data);
}

function postHistoricoInstalacao(data) {
    let url = endpointPr.bd_URL(env.dbpvhistorico);
    gravarDados(url, data);
}

function postCompactarDB(banco){
    let url = endpointPr.dbCompact_URL(banco);
    gravarDados(url);
}

/*=================================================================================
                                        GET                                          
 ==================================================================================*/
function obterInstalacoesAtivas(dtExec) {
    let url = endpointPr.instalacoes_URL(dtExec);
    return obterDados(url);
}

function obterInstalacoesExcluidas() {
    let url = endpointPr.instalacoesExcluidas_URL();
    return obterDados(url);
}

function obterUltimaExecucao() {
    let url = endpointPr.ultimaExecucao_URL();
    return obterDados(url);
}

function obterUltimaAtualizacao() {
    let url = endpointPr.ultimaAtualizacao_URL();
    return obterDados(url);
}

function obterUltimoRegistroHistorico() {
    let url = endpointPr.ultimoRegistroHistorico_URL();
    return obterDados(url);
}

/**==================================== */
function obterInstalacao(npv){
    let url = endpointPr.npvInfo_URL(npv);
    return obterDados(url);
}
module.exports = {
    putPV,
    
    postInstalacao,
    postExecucao,
    postAtualizacaoPV,
    postHistoricoInstalacao,
    postCompactarDB,

    obterInstalacoesAtivas,
    obterUltimaAtualizacao,
    obterInstalacoesExcluidas,
    obterUltimaExecucao,
    obterUltimoRegistroHistorico,

    obterInstalacao,
}