const endpointbase = "http://couchdb-replication-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br/";
const env = getmodule('config/environment').current;

function bd_URL(banco) {
    return endpointbase + banco;
}

function instalacoes_URL(dtExec) {
    banco = env.dbpv;
    if (dtExec === undefined){/**Caso seja primeiro acesso */
        return bd_URL(banco) + "/_design/pv/_view/dtinst";
    }
    else{
        return bd_URL(banco) + "/_design/pv/_view/dtinst?startkey=[\"2017-08-25T00:00-03:00\"]&endkey=[\"" + dtExec + "\"]";
    }
}

/**========================================================================================= */
function ultimaExecucao_URL() {
    banco = env.dbexecucao;
    return bd_URL(banco) + "/_design/exec/_view/dtexec?descending=true&limit=1";
}

function ultimaAtualizacao_URL(){
    banco = env.dbatualizacao;
    return bd_URL(banco) + "/_design/exec/_view/dtexec?descending=true&limit=1";
}

function instalacoesExcluidas_URL() {
    banco = env.dbexclusao;
    return bd_URL(banco) + "/_design/pv/_view/npv?descending=true";
}

function ultimoRegistroHistorico_URL() {
    banco = env.dbpvhistorico;
    console.log(bd_URL(banco)+"/_design/historico/_view/hist?descending=true&limit=1")
    return bd_URL(banco)+"/_design/historico/_view/hist?descending=true&limit=1";
}
/**========================================================================================= */

function npvInfo_URL(npv){
    banco = env.dbpv;
    return bd_URL(banco)+ "/_design/pv/_view/npv?key=["+npv+"]";
}

function npvDoc_URL(doc_id){
    banco = env.dbpv;
    return bd_URL(banco)+"/"+doc_id;
}

/**========================================================================================= */

function dbCompact_URL(banco){
    return bd_URL(banco)+"/_compact";
}

module.exports = {
    bd_URL,
    
    instalacoes_URL,
    ultimaExecucao_URL,
    ultimaAtualizacao_URL,
    instalacoesExcluidas_URL,
    ultimoRegistroHistorico_URL,

    npvInfo_URL,
    npvDoc_URL,
    dbCompact_URL
}