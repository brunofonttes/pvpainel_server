const daoPr = getmodule("db/proprio/dao")

/*=================================================================================
                                     PUT                            
 ==================================================================================*/
function putPV(data, doc_id) {
    data = JSON.stringify(data);
    daoPr.putPV(data, doc_id);
}

async function atualizaDadosPV(pv) {
    try{
        let inst = await obterInstalacao(pv.npv);
        let updtdInst = {
            _rev: inst.rev,
            npv: pv.npv,
            alias: pv.alias,
            pf: pv.pf,
            primeiroAbastecimento: pv.primeiroAbastecimento,
            ultimoAbastecimento: pv.ultimoAbastecimento,
            status: pv.status
        }
        putPV(updtdInst, inst.id);
    }
    catch(err){
        console.log(err);
        return null;
    }   
}
/*=================================================================================
                                     POST                                   
 ==================================================================================*/
function postInstalacao(pv) {
    pv = JSON.stringify(pv);
    daoPr.postInstalacao(pv);
}

function postExecucao(exec) {
    exec = JSON.stringify({ dtexec: exec });
    daoPr.postExecucao(exec);
}

function postHistoricoInstalacao(hist) {
    hist = JSON.stringify(hist);
    daoPr.postHistoricoInstalacao(hist);
}

function postAtualizacaoPV(exec){
    exec = JSON.stringify({ dtexec: exec });
    daoPr.postAtualizacaoPV(exec);
}

function postCompactarDB(banco){
    daoPr.postCompactarDB(banco);
}

/*=================================================================================
                                        GET                                          
 ==================================================================================*/

function obterInstalacoes(doc) {
    return new Promise((resolve, reject) => {
        try {
            doc = JSON.parse(doc);
            doc = doc.rows.map(row => row.value);
            resolve(doc);
        }
        catch (err) {
            resolve(null);
        }
    })
}

async function obterInstalacoesAtivas(dtExec) {
    try{
        let doc = await daoPr.obterInstalacoesAtivas(dtExec);
        return obterInstalacoes(doc);
    }
    catch(err){
        console.log(err);
        return null;
    } 
}

async function obterInstalacoesExcluidas() {
    try{
        let doc = await daoPr.obterInstalacoesExcluidas();
        return obterInstalacoes(doc);
    }
    catch(err){
        console.log(err);
        return null;
    } 
}

async function obterInstalacao(npv) {
    let doc = await daoPr.obterInstalacao(npv);
    return new Promise((resolve, reject) => {
        try {
            doc = JSON.parse(doc);
            let id_rev = {
                id: doc.rows[0].id,
                rev: doc.rows[0].value._rev
            }
            resolve(id_rev);
        }
        catch (err) {
            resolve(null);
        }
    })
}
/*==================================================================================== */
async function obterUltimaExecucao() {
    let doc = await daoPr.obterUltimaExecucao();
    return new Promise((resolve, reject) => {
        try {
            doc = JSON.parse(doc);
            if (doc.rows.length > 0) {
                doc = doc.rows[0].value.dtexec;
                resolve(doc);
            }
            else{
                resolve(0)
            }
        }
        catch (err) {
            resolve(null);
        }
    })
}

async function obterUltimaAtualizacao() {
    let doc = await daoPr.obterUltimaAtualizacao();
    return new Promise((resolve, reject) => {
        try {
            doc = JSON.parse(doc);
            if (doc.rows.length > 0) {
                doc = doc.rows[0].value.dtexec;
                resolve(doc);
            }
            else{
                resolve(0)
            }
        }
        catch (err) {
            resolve(null);
        }
    })
}

async function obterUltimoRegistroHistorico() {
    let doc = await daoPr.obterUltimoRegistroHistorico();
    return new Promise((resolve, reject) => {
        try {
            doc = JSON.parse(doc);
            resolve(doc);
        }
        catch (err) {
            resolve(null);
        }
    })
}

module.exports = {
    atualizaDadosPV,

    postInstalacao,
    postExecucao,
    postAtualizacaoPV,
    postHistoricoInstalacao,
    postCompactarDB,

    obterInstalacoesAtivas,
    obterInstalacoesExcluidas,
    obterUltimaExecucao,
    obterUltimaAtualizacao,
    obterUltimoRegistroHistorico,

    obterInstalacao,
}