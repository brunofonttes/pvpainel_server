const repoPr = getmodule('db/proprio/repository')

module.exports = function (app) {
    return {
        job: function (request, response) {
            io = app.get('io');
            io.once('connection', async function (socket) {
                let fs = require('fs');
                try {
                    let data = await repoPr.obterInstalacoesAtivas();
                    data = data.filter(pv => (pv.status!=-1));
                    let ultExec = await repoPr.obterUltimaExecucao();
                    console.log(data.length);
                    if (data != null || ultExec != null) {
                        if (ultExec != 0) {
                            let json = {
                                hrAtt: ultExec,
                                pvs: data
                            }
                            io.emit('infoPVs', json);
                        }
                    }
                    else {
                        if (data == null) {
                            console.log("Houve um problema durante o acesso a base dbpv.");
                        }
                        if (ultexec == null) {
                            console.log("Houve um problema durante o acesso a base dbexecucao.");
                        }
                    }
                }
                catch (err) {
                    console.log(err);
                }
            })
            response.render('painel');
        },

        obterInstalExt: async function (request, response) {
            const repoPr = getmodule('db/proprio/repository')
            let instalacoesAtivas = await repoPr.obterInstalacoesAtivas();
            response.json(instalacoesAtivas);
        },

        recebedadosPV: function (request, response) {
            let body = "";
            request.on('data', function (data) {
                body += data;
            });

            request.on('end', async function () {
                body = JSON.parse(body);
                repoPr.atualizaDadosPV(body);
            });
            response.writeHead(200, { 'Content-Type': 'text/html' });
            response.end('post received');
        }
    }
}

















