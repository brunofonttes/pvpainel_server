﻿var consign = require('consign')
var express = require('express')
var app = express()
var http = require('http').Server(app);
var io = require('socket.io')(http)
const cron = require('node-cron');

app.set('io', io);
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

global.getmodule = function getmodule(modulePath) {
    var path = require('path');
    return require(path.resolve(modulePath));
}

consign()
    .include('controllers')
    .then('routes')
    .into(app);

var port = process.env.PORT || 3006;

http.listen(port, function () {
    var atzLPv = getmodule('jobs/atualizacaoLPV');
    var bpa = getmodule('jobs/buscaPorAbastecimento');
    var rdh = getmodule('jobs/registroDeHistorico');
    var cptLPv = getmodule('jobs/compactacaoLPV')

    // atzLPv.atualizacaoLPV()
    bpa.buscaPorAbastecimento(io);
    const atzLpvTask = cron.schedule('*/29 * * * *', function () { atzLPv.atualizacaoLPV() })
    const bpaTask = cron.schedule('*/10 * * * *', function () { bpa.buscaPorAbastecimento(io) })
    const rdhTask = cron.schedule('* 18 * * *', function () { rdh.registroDeHistorico() })
    const cptLpvTask = cron.schedule('* 1 * * *', function () { cptLPv.compactacaoLPV() })

    atzLpvTask.start();
    bpaTask.start();

    rdhTask.start();
    cptLpvTask.start();

    console.log('Servidor rodando em http://localhost:%s', port);
});


module.exports = app

